include .env

all: makemigrations migrate dev

migrate:
	pipenv run python src/manage.py migrate $(if $m, api $m,)

migrate_docker:
	docker-compose run web pipenv run python src/manage.py migrate

makemigrations:
	python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

makemigrations_docker:
	docker-compose exec web pipenv run python src/manage.py makemigrations

createsuperuser:
	python src/manage.py createsuperuser

createsuperuser_docker:
	docker-compose exec web pipenv run python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

collectstatic_docker:
	docker-compose run web pipenv run python src/manage.py collectstatic --noinput --clear

dev:
	python src/manage.py runserver 0.0.0.0:8000

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

docker_lint:
	docker-compose run web pipenv run isort --check --diff .
	docker-compose run web pipenv run flake8 --config setup.cfg
	docker-compose run web pipenv run black --check --config pyproject.toml .

start_bot:
	python src/manage.py start_bot

up:
	docker-compose up

up_detach:
	docker-compose up -d

down:
	docker-compose down

build:
	docker-compose build

pull:
	docker pull ${IMAGE_APP}

push:
	docker push ${IMAGE_APP}

test:
	docker-compose run web pipenv run bash -c "cd ./src && pipenv run pytest"
