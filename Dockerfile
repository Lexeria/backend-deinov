FROM python:3.10-slim-buster

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

WORKDIR /app

COPY Pipfile Pipfile.lock ./

RUN pip install update pip && pip install --no-cache-dir pipenv && pipenv install 

COPY . .
