import pytest

from app.internal.users.db.models import User


@pytest.mark.unit
@pytest.mark.django_db
def test_add_user_in_db(user, user_repo):
    _, created = user_repo.update_or_create_user(
        user.id, user.is_bot, user.username, user.first_name, user.last_name, user.language_code
    )
    # TODO: fix this
    # assert created

    model_users = User.objects.filter(id=user.id).all()
    assert model_users.count() == 1

    model_user = model_users[0]
    assert model_user.username == user.username
    assert model_user.first_name == user.first_name


@pytest.mark.unit
@pytest.mark.django_db
def test_update_user_in_db(user, user_repo):
    _, created = user_repo.update_or_create_user(
        user.id, user.is_bot, user.username, user.first_name, user.last_name, user.language_code
    )
    assert not created
