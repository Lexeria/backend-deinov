import pytest

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.users.db.models import User
from app.internal.users.db.repositories import UserRepository


@pytest.fixture
def user(id=1, is_bot=False, first_name="Первый", username="first"):
    cur_user = User.objects.create(id=id, is_bot=is_bot, first_name=first_name, username=username)
    print(
        cur_user,
        cur_user.id,
        cur_user.is_bot,
        cur_user.username,
        cur_user.first_name,
        cur_user.last_name,
        cur_user.language_code,
    )
    return cur_user


@pytest.fixture
def second_user(id=2, is_bot=False, first_name="Второй", username="second"):
    return User.objects.create(id=id, is_bot=is_bot, second_name=first_name, username=username)


@pytest.fixture
def account(id=3, is_bot=False, first_name="Со счетом", username="account", number="1" * 20, amount=1000):
    user = User.objects.create(id=id, is_bot=is_bot, second_name=first_name, username=username)
    return BankAccount.objects.create(number=number, owner=user, amount=amount)


@pytest.fixture
def second_account(
    id=4, is_bot=False, first_name="Со вторым счетом", username="account_2", number="2" * 20, amount=1000
):
    user = User.objects.create(id=id, is_bot=is_bot, second_name=first_name, username=username)
    return BankAccount.objects.create(number=number, owner=user, amount=amount)


@pytest.fixture
@pytest.mark.django_db
def user_repo():
    return UserRepository()
