import pytest
from telegram import Update

from app.internal.bot.bot import get_bot


@pytest.mark.smoke
def test_tg_bot():
    updater = get_bot()
    updater.dispatcher.process_update(Update.de_json({}, updater.bot))
