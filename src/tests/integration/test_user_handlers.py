from unittest.mock import MagicMock

import pytest

from app.internal.bot.handlers import start
from app.internal.users.db.models import User


@pytest.mark.django_db
@pytest.mark.integration
def test_start(update: MagicMock, user) -> None:
    start(update, None)

    assert User.objects.filter(id=user.id, first_name=user.first_name, username=user.username).exists()
