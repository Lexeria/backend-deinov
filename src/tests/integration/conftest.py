from unittest.mock import MagicMock

import pytest


@pytest.fixture
def update(user):
    message = MagicMock()
    message.reply_text.return_value = None
    message.text = ""

    update = MagicMock()
    update.effective_user = user
    update.message = message

    return update


@pytest.fixture
def context():
    context = MagicMock()
    context.args = []
    context.user_data = dict()

    return context
