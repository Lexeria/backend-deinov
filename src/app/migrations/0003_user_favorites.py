# Generated by Django 3.2.12 on 2022-04-20 22:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0002_auto_20220323_0311"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="favorites",
            field=models.ManyToManyField(to="app.User"),
        ),
    ]
