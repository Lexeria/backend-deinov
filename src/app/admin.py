from django.contrib import admin

from app.internal.admins.presentation.admin import AdminUserAdmin
from app.internal.auth.presentation.admin import IssuedToken
from app.internal.bank_accounts.presentation.admin import BankAccountAdmin
from app.internal.bank_transactions.presentation.admin import BankTransactionAdmin
from app.internal.cards.presentation.admin import CardAdmin
from app.internal.users.presentation.admin import UserAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
