def serialize_user(user):
    return dict(
        id=user.id,
        username=user.username,
        is_bot=user.is_bot,
        first_name=user.first_name,
        last_name=user.last_name,
        language_code=user.language_code,
        phone_number=str(user.phone_number),
    )
