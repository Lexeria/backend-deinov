from django.db import models

from app.internal.bank_accounts.db.models import BankAccount


class Card(models.Model):
    number = models.CharField(max_length=18, unique=True)
    bank_account = models.ForeignKey(BankAccount, on_delete=models.CASCADE)
