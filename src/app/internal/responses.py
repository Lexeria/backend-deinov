from ninja import Schema


class OkResponse(Schema):
    pass


# class CreatedResponse(Schema):
#     pass


class ErrorResponse(Schema):
    message: str
