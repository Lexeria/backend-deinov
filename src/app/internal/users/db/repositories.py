import hashlib
import hmac
from typing import Optional, Tuple

from django.conf import settings

from app.internal.bot.replies import (
    ADD_FAVORITE,
    FAVORITE_EXIST,
    FAVORITE_NOT_EXIST,
    REMOVE_FAVORITE,
    SELF_FAVORITE,
    SET_PASSWORD_ERROR,
    SET_PASSWORD_SUCCESS,
    USER_NOT_EXIST,
)
from app.internal.users.db.models import User


class UserRepository:
    def update_or_create_user(
        id: int, is_bot: bool, username: str, first_name: str, last_name: Optional[str], language_code: Optional[str]
    ) -> Tuple[User, bool]:
        # TODO: вынести в defaults
        return User.objects.update_or_create(
            id=id,
            is_bot=is_bot,
            username=username,
            first_name=first_name,
            last_name=last_name,
            language_code=language_code,
        )

    def find_user(id: int, values=[]) -> Optional[User]:
        return (
            User.objects.filter(id=id).values(*values).first()
            if len(values) != 0
            else User.objects.filter(id=id).first()
        )

    def find_user_by_username(username: str) -> Optional[User]:
        return User.objects.filter(username=username).first()

    def save_user_phone_number(user: User, phone_number: str):
        user.phone_number = phone_number
        user.save()

    def get_favorites(user: User):
        usernames_from_favorites = user.favorites.values_list("username", flat=True)
        return list(usernames_from_favorites)

    def add_user_favorite(user: User, favorite_username: str) -> str:
        try:
            if user.username.lower() == favorite_username.lower():
                return SELF_FAVORITE
            if user.favorites.filter(username__iexact=favorite_username).exists():
                return FAVORITE_EXIST(favorite_username)
            favorite_user = User.objects.get(username__iexact=favorite_username)
            user.favorites.add(favorite_user)
            user.save()
            return ADD_FAVORITE(favorite_username)
        except User.DoesNotExist:
            return USER_NOT_EXIST(favorite_username)

    def remove_user_favorite(user: User, favorite_username: str) -> str:
        try:
            if not user.favorites.filter(username__iexact=favorite_username).exists():
                return FAVORITE_NOT_EXIST(favorite_username)
            favorite_user = User.objects.get(username__iexact=favorite_username)
            user.favorites.remove(favorite_user)
            user.save()
            return REMOVE_FAVORITE(favorite_username)
        except User.DoesNotExist:
            return USER_NOT_EXIST(favorite_username)

    def get_password_hash(password: str) -> str:
        return hmac.new((settings.JWT_SECRET).encode(), password.encode(), hashlib.sha256).hexdigest()

    def set_user_password(self, user: User, password: str) -> str:
        try:
            user.password = self.get_password_hash(password)
            user.save()
            return SET_PASSWORD_SUCCESS
        except:
            return SET_PASSWORD_ERROR
