from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class User(models.Model):
    id = models.PositiveBigIntegerField(primary_key=True)
    is_bot = models.BooleanField()
    username = models.CharField(max_length=32, blank=True, unique=True, null=True)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64, blank=True, null=True)
    language_code = models.CharField(max_length=3, blank=True, null=True)
    phone_number = PhoneNumberField(blank=True, null=True)
    favorites = models.ManyToManyField("self", symmetrical=False)
    password = models.CharField(max_length=256, null=True)

    def __str__(self):
        return str(self.username)

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"
