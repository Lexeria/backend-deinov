from ninja import Router

from app.internal.responses import ErrorResponse, OkResponse
from app.internal.users.db.repositories import UserRepository
from app.internal.users.presentation.entities import UserOut
from app.internal.users.presentation.handlers import UserHandlers


def get_users_router(user_handlers: UserHandlers):
    router = Router(tags=["users"])

    router.add_api_operation(
        "/me",
        ["GET"],
        user_handlers.me,
        response={200: UserOut, 401: ErrorResponse},
    )

    router.add_api_operation(
        "/set_phone",
        ["POST"],
        user_handlers.set_phone,
        response={200: UserOut, 400: ErrorResponse, 401: ErrorResponse},
    )

    router.add_api_operation(
        "/favorites", ["POST"], user_handlers.add_favorite_user, response={201: OkResponse, 400: ErrorResponse}
    )

    router.add_api_operation(
        "/favorites",
        ["DELETE"],
        user_handlers.delete_favorite_user,
        response={200: OkResponse, 400: ErrorResponse, 404: ErrorResponse},
    )

    return router


def add_users_router(api, user_handlers: UserHandlers):
    users_router = get_users_router(user_handlers)
    api.add_router("/users", users_router)


def add_users_api(api):
    user_repo = UserRepository()
    user_handler = UserHandlers(user_repo)
    add_users_router(api, user_handler)
