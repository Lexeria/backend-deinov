from ninja import Body

from app.internal.exceptions.exceptions import NotFoundException
from app.internal.responses import OkResponse
from app.internal.users.presentation.entities import UserOut


class UserHandlers:
    def __init__(self, user_repository):
        self._user_repo = user_repository

    def me(self, request):
        user = self._user_repo.find_user(request.user_id)
        return UserOut.from_orm(user)

    def set_phone(self, request, phone_number: str = Body(...)):
        try:
            user = self._user_repo.find_user(request.user_id)
            self._user_repo.set_phone(user, phone_number)
        except:
            raise NotFoundException(request.user_id, "user")

    def add_favorite_user(self, request, username):
        user = self._user_repo.find_user(request.user_id)
        if not self._user_repo.add_user_favorite(user, username):
            raise NotFoundException(username, "user")

        return OkResponse()

    def delete_favorite_user(self, request, username):
        user = self._user_repo.find_user(request.user_id)
        if not self._user_repo.remove_user_favorite(user, username):
            raise NotFoundException(username, "favorite_user")

        return OkResponse()
