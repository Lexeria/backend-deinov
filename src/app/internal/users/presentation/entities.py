from uuid import UUID

from ninja.orm import create_schema

from app.internal.users.db.models import User

UserSchema = create_schema(User, exclude=["id", "password"])


class UserOut(UserSchema):
    id: UUID
