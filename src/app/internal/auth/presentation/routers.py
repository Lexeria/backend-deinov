from ninja import Router

from app.internal.auth.db.repositories import AuthRepository
from app.internal.auth.presentation.entities import JWTSchema
from app.internal.auth.presentation.handlers import AuthHandlers
from app.internal.responses import ErrorResponse
from app.internal.users.db.repositories import UserRepository


def get_auth_router(auth_handlers: AuthHandlers):
    router = Router(tags=["auth"])

    router.add_api_operation(
        "/login",
        ["POST"],
        auth_handlers.login,
        auth=None,
        response={200: JWTSchema, 401: ErrorResponse},
    )

    router.add_api_operation(
        "/update_tokens",
        ["POST"],
        auth_handlers.refresh,
        auth=None,
        response={200: JWTSchema, 400: ErrorResponse},
    )

    return router


def add_auth_router(api, auth_handlers: AuthHandlers):
    auth_router = get_auth_router(auth_handlers)
    api.add_router("/auth", auth_router)


def add_auth_api(api):
    auth_repo = AuthRepository()
    user_repo = UserRepository()
    auth_handler = AuthHandlers(auth_repo, user_repo)
    add_auth_router(api, auth_handler)
