from ninja import Schema


class JWTSchema(Schema):
    access_token: str
    refresh_token: str


class LoginSchema(Schema):
    username: str
    password: str
