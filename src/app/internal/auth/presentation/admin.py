from django.contrib import admin

from app.internal.auth.db.models import IssuedToken


@admin.register(IssuedToken)
class IssuedToken(admin.ModelAdmin):
    pass
