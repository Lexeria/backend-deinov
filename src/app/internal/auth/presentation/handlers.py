from ninja import Body

from app.internal.auth.presentation.entities import JWTSchema, LoginSchema
from app.internal.auth.presentation.replies import EMPTY_TOKEN, EXPIRED_TOKEN, REVOKED_TOKEN
from app.internal.exceptions.exceptions import BadRequestException, UnauthorizedException

ONE_HOUR = 1 * 60 * 60
TWELVE_HOURS = 12 * ONE_HOUR


class AuthHandlers:
    def __init__(self, auth_repository, user_repository):
        self._auth_repo = auth_repository
        self._user_repo = user_repository

    def login(self, request, login: LoginSchema = Body(...)):
        user = self._user_repo.find_user_by_username(login.username)
        password_hash = self._user_repo.get_password_hash(login.password)
        if not (user and user.password and user.password == password_hash):
            return UnauthorizedException()

        access_token_ttl = ONE_HOUR
        refresh_token_ttl = TWELVE_HOURS
        tokens = self._auth_repo.generate_tokens(
            user_id=user.id, access_token_ttl=access_token_ttl, refresh_token_ttl=refresh_token_ttl
        )
        return JWTSchema(access_token=tokens["access_token"], refresh_token=tokens["refresh_token"])

    def refresh(self, request, refresh_token):
        token = self._auth_repo.get_token(refresh_token)
        if not token:
            return BadRequestException(EMPTY_TOKEN)

        if token.revoked:
            self._auth_repo.revoke_all_tokens(token.user.id)
            return BadRequestException(REVOKED_TOKEN)

        self._auth_repo.revoke_token(token.jti)
        if self._auth_repo.is_token_expired(token.jti):
            return BadRequestException(EXPIRED_TOKEN)

        access_token_ttl = ONE_HOUR
        refresh_token_ttl = TWELVE_HOURS
        tokens = self._auth_repo.generate_tokens(
            user_id=token.user.id, access_token_ttl=access_token_ttl, refresh_token_ttl=refresh_token_ttl
        )
        return JWTSchema(access_token=tokens["access_token"], refresh_token=tokens["refresh_token"])
