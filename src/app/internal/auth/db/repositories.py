from datetime import datetime

import jwt
from django.conf import settings
from django.db import transaction

from app.internal.auth.db.models import IssuedToken


class AuthRepository:
    def generate_tokens(user, access_token_ttl: int, refresh_token_ttl: int):
        access_token = jwt.encode(
            {"user_id": user.id, "expires": datetime.now().timestamp() + access_token_ttl},
            settings.JWT_SECRET,
            algorithm="HS256",
        )
        refresh_token = jwt.encode(
            {"expires": datetime.now().timestamp() + refresh_token_ttl}, settings.JWT_SECRET, algorithm="HS256"
        )

        with transaction.atomic():
            IssuedToken.objects.create(jti=refresh_token, user=user)

        return {"access_token": access_token, "refresh_token": refresh_token}

    def get_token(refresh_token):
        return IssuedToken.objects.filter(jti=refresh_token).first()

    def is_token_expired(refresh_token):
        try:
            payload = jwt.decode(refresh_token, settings.JWT_SECRET, algorithms=["HS256"])

            time = payload.get("expires")
            return time < datetime.now().timestamp() or time is None
        except jwt.exceptions.ExpiredSignatureError:
            return True

    def revoke_token(self, refresh_token):
        token = self.get_token(refresh_token)
        token.revoked = True
        token.save()

    def revoke_all_tokens(user):
        user.refresh_tokens.update(revoked=True)
        user.save()
