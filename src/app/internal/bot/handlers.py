import decimal

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.bank_accounts.db.repositories import BankAccountRepository
from app.internal.bot.replies import (
    ADD_PHONE_NUMBER,
    BALANCE,
    EMPTY_AMOUNT,
    EMPTY_FAVORITE,
    EMPTY_HISTORY_ACCOUNT_NUMBER,
    EMPTY_PASSWORD,
    EMPTY_PHONE_NUMBER,
    EMPTY_USERNAME,
    FAVORITES,
    HELP,
    ME,
    NOT_IN_DB,
    START,
    SUCCESS_SET_PHONE_NUMBER,
    UNCORRECT_PHONE_NUMBER,
    UNCORRECT_SUM,
    UNKNOWN,
)
from app.internal.users.db.repositories import UserRepository

user_repo = UserRepository()
bank_account_repo = BankAccountRepository()


def start(update: Update, context: CallbackContext) -> None:
    user = update.effective_user
    _, created = user_repo.update_or_create_user(
        id=user.id,
        is_bot=user.is_bot,
        username=user.username,
        first_name=user.first_name,
        last_name=user.last_name,
        language_code=user.language_code,
    )
    update.message.reply_text(START(user.first_name, created))


def help(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(HELP)


def try_get_db_user(update: Update, values=[]):
    user = update.effective_user
    db_user = user_repo.find_user(user.id, values)

    # TODO: вынести проверки в декораторы
    if not db_user:
        update.message.reply_text(NOT_IN_DB)
        return

    phone_number = db_user["phone_number"] if len(values) != 0 else db_user.phone_number
    if not phone_number:
        update.message.reply_text(ADD_PHONE_NUMBER)
        return

    return db_user


def set_phone(update: Update, context: CallbackContext) -> None:
    user = update.effective_user
    db_user = user_repo.find_user(user.id)

    if not db_user:
        update.message.reply_text(NOT_IN_DB)
        return

    if not len(context.args):
        update.message.reply_text(EMPTY_PHONE_NUMBER)
        return
    phone_number = context.args[0]

    try:
        user_repo.save_user_phone_number(db_user, phone_number)
        update.message.reply_text(SUCCESS_SET_PHONE_NUMBER)
    except:
        update.message.reply_text(UNCORRECT_PHONE_NUMBER)


def me(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update)
    if not db_user:
        return
    amount_numbers = bank_account_repo.get_account_numbers(db_user.id)

    update.message.reply_text(
        ME(
            db_user.id,
            db_user.username,
            db_user.first_name,
            db_user.last_name,
            db_user.language_code,
            db_user.phone_number,
            amount_numbers,
        ),
        parse_mode="MarkdownV2",
    )


# TODO: Вынести логику
def balance(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update, ["id", "phone_number"])
    if not db_user:
        return

    balance = bank_account_repo.get_user_balance(db_user["id"])
    answer = BALANCE(balance)
    update.message.reply_text(answer, parse_mode="MarkdownV2")


def favorites(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update)
    if not db_user:
        return

    user_favorites = user_repo.get_favorites(db_user)
    answer = FAVORITES(user_favorites)
    update.message.reply_text(answer, parse_mode="MarkdownV2")


def add_favorite(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update)
    if not db_user:
        return

    if not len(context.args):
        update.message.reply_text(EMPTY_FAVORITE("add"))
        return
    username = context.args[0].replace("@", "")

    result = user_repo.add_user_favorite(db_user, username)
    update.message.reply_text(result)


def remove_favorite(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update)
    if not db_user:
        return

    if not len(context.args):
        update.message.reply_text(EMPTY_FAVORITE("remove"))
        return
    username = context.args[0].replace("@", "")

    result = user_repo.remove_user_favorite(db_user, username)
    update.message.reply_text(result)


def transfer_by_amount(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update, values=["id", "phone_number"])
    if not db_user:
        return

    if len(context.args) != 3:
        update.message.reply_text(EMPTY_AMOUNT)
        return
    from_number, to_number, summa = context.args

    try:
        result = bank_account_repo.user_transfer_by_account(
            db_user["id"], from_number, to_number, decimal.Decimal(str(summa).replace(",", "."))
        )
        update.message.reply_text(result)
    except decimal.InvalidOperation:
        update.message.reply_text(UNCORRECT_SUM)


def transfer_by_username(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update, ["id", "username", "phone_number"])
    if not db_user:
        return

    if len(context.args) != 3:
        update.message.reply_text(EMPTY_USERNAME)
        return
    from_number, to_username, summa = context.args

    try:
        result = bank_account_repo.user_transfer_by_username(
            db_user["username"],
            db_user["id"],
            from_number,
            to_username.replace("@", ""),
            decimal.Decimal(str(summa).replace(",", ".")),
        )
        update.message.reply_text(result)
    except decimal.InvalidOperation:
        update.message.reply_text(UNCORRECT_SUM)


def history(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update, ["id", "phone_number"])
    if not db_user:
        return

    if not len(context.args):
        update.message.reply_text(EMPTY_HISTORY_ACCOUNT_NUMBER)
        return

    result = bank_account_repo.user_history(db_user["id"], context.args[0])
    update.message.reply_text(
        result,
        parse_mode="MarkdownV2",
    )


def interacting_persons(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update)
    if not db_user:
        return

    result = bank_account_repo.user_interacting_persons(db_user)
    update.message.reply_text(
        result,
        parse_mode="MarkdownV2",
    )


def unknown_handler(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(UNKNOWN)


def set_password(update: Update, context: CallbackContext) -> None:
    db_user = try_get_db_user(update)
    if not db_user:
        return

    if not len(context.args):
        update.message.reply_text(EMPTY_PASSWORD)
        return
    password = context.args[0]

    result = user_repo.set_user_password(db_user, password)
    update.message.reply_text(result)
