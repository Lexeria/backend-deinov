#!/usr/bin/env python
import environ
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater

from app.internal.bot.handlers import (
    add_favorite,
    balance,
    favorites,
    help,
    history,
    interacting_persons,
    me,
    remove_favorite,
    set_password,
    set_phone,
    start,
    transfer_by_amount,
    transfer_by_username,
    unknown_handler,
)

env = environ.Env(
    TOKEN=(str, ""),  # TODO: убрать в другое место
)
TOKEN = env("TOKEN")


def get_bot():
    updater = Updater(token=TOKEN)

    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help))

    dispatcher.add_handler(CommandHandler("set_phone", set_phone))
    dispatcher.add_handler(CommandHandler("set_password", set_password))
    dispatcher.add_handler(CommandHandler("me", me))
    dispatcher.add_handler(CommandHandler("balance", balance))

    dispatcher.add_handler(CommandHandler("favorites", favorites))
    dispatcher.add_handler(CommandHandler("add_favorite", add_favorite))
    dispatcher.add_handler(CommandHandler("remove_favorite", remove_favorite))

    dispatcher.add_handler(CommandHandler("transfer_by_amount", transfer_by_amount))
    dispatcher.add_handler(CommandHandler("transfer_by_username", transfer_by_username))

    dispatcher.add_handler(CommandHandler("history", history))
    dispatcher.add_handler(CommandHandler("interacting_persons", interacting_persons))

    dispatcher.add_handler(MessageHandler(Filters.text | Filters.command, unknown_handler))

    return updater


def start_bot():
    updater = get_bot()
    updater.start_webhook(
        listen="0.0.0.0",
        port=8443,
        url_path=TOKEN,
        webhook_url=f"https://lexeria.backend22.2tapp.cc/{TOKEN}",
    )
    updater.idle()


if __name__ == "__main__":
    start_bot()
