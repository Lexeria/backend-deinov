SELF_FAVORITE = "Вы, конечно, избраны, но не для этого списка"


def FAVORITE_EXIST(username):
    return f"{username} уже в списке избранных"


def ADD_FAVORITE(username):
    return f"{username} добавлен в список избранных"


def FAVORITE_NOT_EXIST(username):
    return f"{username} нет в списке избранных"


def REMOVE_FAVORITE(username):
    return f"{username} удален из списка избранных"


def USER_NOT_EXIST(username):
    return f"Пользователь с username @{username} не найден"


EQUAL_NUMBER = "Если хотите перевести себе, введите счет отличный от счета для перевода"

NEGATIVE_SUM = "Слушай, а ловко ты это придумал. Я даже сразу и не понял, молодец"

NO_MONEY_ENOUGH = "Мы не обслуживаем бедняков, пожалуйста, пополните счет"


def SUCCESS_TRANSFER(amount):
    return f"Перевод совершен успешно, ваш текущий баланс: {amount}₽"


def NUMBER_NOT_EXIST(number):
    return f"Мы не нашли владельца счета {number}, проверьте введенный номер и попробуйте еще раз"


def UNCORRECT_SELF_NUMBER(number):
    return f"Мы не обслуживаем номер счета {number} на ваше имя, попробуйте ввести другой номер"


EQUAL_USERNAME = "Если хотите перевести себе, воспользуйтесь лучше переводом по счету"


def USERNAME_NOT_EXIST(username):
    return f"Мы не нашли пользователя @{username}, проверьте введенное имя и попробуйте еще раз"


def USER_HAVE_NO_ACCOUNT(username):
    return f"У пользователя @{username} нет счета для перевода"


def HISTORY_FOR_NUMBER(number):
    return f"История переводов для счета *№{number}*:\n"


def HISTORY_OPERATION(date, number, sign, amount):
    return (
        date.strftime("%Y-%m-%d %H:%M:%S").replace("-", "\\-")
        + f": *\\{sign}{str(amount).replace('.', ',')}₽* \\(_перевод {'для' if sign == '-' else 'от'} счёта №{number}_\\)"
    )


def HISTORY_BALANCE(balance):
    return f"\nТекущий баланс: __{str(balance).replace('.', ',')}₽__"


def INTERACTING_USERS(usernames):
    if len(usernames) == 0:
        return "Вы еще ни с кем *не взаимодействовали* :С"
    return "*Взаимодействие было с:*\n" + "\n".join(map(lambda x: f"@{x}", usernames))


SET_PASSWORD_SUCCESS = "Пароль изменен"

SET_PASSWORD_ERROR = "Ошибка при установке пароля"


HELP = (
    "Используй /start для добавления или обновления информации о себе\n\n"
    "/set_phone [phone_number] поможет добавить или обновить твой номер телефона (без него тебе другие команды будут недоступны)\n"
    "/set_password [password] поможет добавить или обновить твой пароль\n"
    "/me отобразит текущую информацию о тебе, которую знают коллекторы\n"
    "/balance поможет узнать, сколько денег на счетах\n\n"
    "/favorites покажет текущий список избранных контактов\n"
    "/add_favorite [@username] добавит пользователя в список избранных\n"
    "/remove_favorite [@username] удалить пользователя из списка избранного\n\n"
    "/transfer_by_amount [from_amount_number] [to_amount_number] для перевода по номеру счета\n"
    "/transfer_by_username [from_amount_number] [@username] для перевода по username\n\n"
    "/history [your_account_number] покажет вашу историю переводов\n"
    "/interacting_persons расскажет, с какими людьми было взаимодействие"
)


def START(first_name, created):
    return f"Привет, {first_name}! {'Добавил' if created else 'Обновил'} информацию о тебе"


NOT_IN_DB = "Тебя нет в базе, используй /start для начала\nВсе команды можно узнать с помощью /help"

ADD_PHONE_NUMBER = "Добавь свой номер телефона, чтобы пользоваться другими командами"

EMPTY_PHONE_NUMBER = "Введите номер телефона после команды /set_phone\nНапример, /set_phone 88005553535"

SUCCESS_SET_PHONE_NUMBER = "Данные улетели коллектору…"

UNCORRECT_PHONE_NUMBER = "Введи корректный номер телефона :С"


def ME(id, username, first_name, last_name, language_code, phone_number, amount_numbers):
    lines = [
        "Вот что коллекторам известно о тебе:\n",
        f"*id:* {id}\n",
        f"*Username:* {username}\n" if username else "",
        f"*Имя:* {first_name}\n",
        f"*Фамилия:* {last_name}\n" if last_name else "",
        f"*Язык:* {language_code}\n" if language_code else "",
        f"*Телефон:* {phone_number}\n".replace("+", "\\+") if phone_number else "",
        "*Номера счетов:*\n" + "\n".join(list(map(lambda x: "— " + x, amount_numbers)))
        if amount_numbers and len(amount_numbers) != 0
        else "У вас *нет счетов*",
    ]
    return "".join(lines)


def BALANCE(balance):
    return (
        "*Текущий баланс:*\n" + "\n".join(list(map(lambda x: f"• Счет №{x[0]} \\(баланс: {x[1]}₽\\)", balance)))
        if balance and len(balance) != 0
        else "У вас *нет счетов*, коллекторам грустно :С"
    )


def FAVORITES(user_favorites):
    return (
        "*Ваши сообщники:*\n" + "\n".join(map(lambda x: "@" + x, user_favorites))
        if len(user_favorites) != 0
        else "У вас *нет сообщников*"
    )


def EMPTY_FAVORITE(command):
    return f"Введите username после команды /{command}_favorite\nНапример, /{command}_favorite @Lexeria"


EMPTY_AMOUNT = "Введите ваш номер счета, счет получателя и сумму перевода после команды /transfer_by_amount\nНапример, /transfer_by_amount 0000 1111 500"

UNCORRECT_SUM = "Введите корректную сумму"

EMPTY_USERNAME = "Введите ваш номер счета, username получателя и сумму перевода после команды /transfer_by_username\nНапример, /transfer_by_username 0000 @Lexeria 500"

EMPTY_HISTORY_ACCOUNT_NUMBER = "Введите ваш номер счета после команды /history\nНапример, /history 69696969"

UNKNOWN = "Прости, но я тебя не понимаю :(\nВсе доступные команды можно узнать с помощью /help"

EMPTY_PASSWORD = "Введите пароль после команды /set_password\nНапример, /set_password 69696969"
