from ninja import NinjaAPI

from app.internal.auth.presentation.auth import HTTPJWTAuth
from app.internal.auth.presentation.routers import add_auth_api
from app.internal.exceptions.handlers import add_exception_handlers
from app.internal.users.presentation.routers import add_users_api


def get_api():
    api = NinjaAPI(
        title="backend",
        version="1.0.0",
        auth=[HTTPJWTAuth()],
    )

    add_exception_handlers(api)
    add_auth_api(api)
    add_users_api(api)

    return api
