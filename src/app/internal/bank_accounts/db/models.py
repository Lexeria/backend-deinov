from django.db import models

from app.internal.users.db.models import User


class BankAccount(models.Model):
    number = models.CharField(max_length=20, unique=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="bank_accounts")
    amount = models.DecimalField(default=0, max_digits=20, decimal_places=2)

    def __str__(self):
        return self.number
