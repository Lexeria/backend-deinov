from typing import Optional

from django.db import transaction
from django.db.models import F, Q
from django.utils import timezone

from app.internal.bank_accounts.db.models import BankAccount
from app.internal.bank_transactions.db.models import BankTransaction
from app.internal.bot.replies import (
    EQUAL_NUMBER,
    EQUAL_USERNAME,
    HISTORY_BALANCE,
    HISTORY_FOR_NUMBER,
    HISTORY_OPERATION,
    INTERACTING_USERS,
    NEGATIVE_SUM,
    NO_MONEY_ENOUGH,
    NUMBER_NOT_EXIST,
    SUCCESS_TRANSFER,
    UNCORRECT_SELF_NUMBER,
    USER_HAVE_NO_ACCOUNT,
    USERNAME_NOT_EXIST,
)
from app.internal.users.db.models import User


class BankAccountRepository:
    def get_bank_account(self, account_number):
        return BankAccount.objects.filter(number=account_number).first()

    def get_account_numbers(user_id: str):
        account_numbers = BankAccount.objects.filter(owner__id=user_id).values_list("number", flat=True)
        return list(account_numbers)

    def get_account_balance(account_number: str) -> Optional[int]:
        balance = BankAccount.objects.filter(number=account_number).values("amount").first()
        if balance:
            return str(balance["amount"]).replace(".", ",")

    def get_user_balance(self, user_id: str):
        account_numbers = self.get_account_numbers(user_id)
        if len(account_numbers) != 0:
            return list(map(lambda x: [x, self.get_account_balance(x) or 0], account_numbers))

    def user_transfer_by_account(user_id: str, from_number: str, to_number: str, summa) -> str:
        if from_number == to_number:
            return EQUAL_NUMBER
        if summa < 0:
            return NEGATIVE_SUM

        with transaction.atomic():
            from_account = BankAccount.objects.select_for_update().filter(owner__id=user_id, number=from_number).first()
            if not from_account:
                return UNCORRECT_SELF_NUMBER(from_number)
            from_amount = from_account.amount
            if from_amount < summa:
                return NO_MONEY_ENOUGH

            to_account = BankAccount.objects.select_for_update().filter(number=to_number).first()
            if not to_account:
                return NUMBER_NOT_EXIST(to_number)

            from_account.amount = F("amount") - summa
            from_account.save(update_fields=("amount",))
            to_account.amount = F("amount") + summa
            to_account.save(update_fields=("amount",))
            BankTransaction.objects.create(from_account=from_account, to_account=to_account, amount=summa)

            return SUCCESS_TRANSFER(from_amount - summa)

    def user_transfer_by_username(user_username: str, user_id: int, from_number: str, to_username: str, summa) -> str:
        if user_username.lower() == to_username.lower():
            return EQUAL_USERNAME
        if summa < 0:
            return NEGATIVE_SUM
        try:
            to_user = User.objects.get(username__iexact=to_username)

            with transaction.atomic():
                from_account = (
                    BankAccount.objects.select_for_update().filter(owner__id=user_id, number=from_number).first()
                )
                if not from_account:
                    return UNCORRECT_SELF_NUMBER(from_number)
                from_amount = from_account.amount
                if from_amount < summa:
                    return NO_MONEY_ENOUGH

                to_account = BankAccount.objects.select_for_update().filter(owner=to_user).first()
                if not to_account:
                    return USER_HAVE_NO_ACCOUNT(to_username)
                from_account.amount = F("amount") - summa
                from_account.save(update_fields=("amount",))
                to_account.amount = F("amount") + summa
                to_account.save(update_fields=("amount",))
                BankTransaction.objects.create(
                    date=timezone.now(), from_account=from_account, to_account=to_account, amount=summa
                )

                return SUCCESS_TRANSFER(from_amount - summa)
        except User.DoesNotExist:
            return USERNAME_NOT_EXIST(to_username)

    def user_history(self, user_id: str, account_number: str) -> str:
        if not BankAccount.objects.filter(owner__id=user_id, number=account_number).exists():
            return UNCORRECT_SELF_NUMBER(account_number)

        transactions = (
            BankTransaction.objects.filter(
                Q(from_account__number=account_number) | Q(to_account__number=account_number)
            )
            .order_by("-date")
            .values("date", "from_account__number", "to_account__number", "amount")
        )
        history = [HISTORY_FOR_NUMBER(account_number)]
        for ops in transactions:
            to_user_transaction = ops["to_account__number"] == account_number
            sign = "+" if to_user_transaction else "-"
            history.append(
                HISTORY_OPERATION(
                    ops["date"],
                    ops["from_account__number"] if to_user_transaction else ops["to_account__number"],
                    sign,
                    ops["amount"],
                )
            )
        history.append(HISTORY_BALANCE(self.get_account_balance(account_number)))

        return "\n".join(history)

    def user_interacting_persons(user: User) -> str:
        usernames_from = user.bank_accounts.values_list(
            "transactions_to__from_account__owner__username", flat=True
        ).distinct()
        usernames_to = user.bank_accounts.values_list(
            "transactions_from__to_account__owner__username", flat=True
        ).distinct()
        usernames = usernames_from.union(usernames_to)

        return INTERACTING_USERS(list(filter(lambda x: x, usernames)))
