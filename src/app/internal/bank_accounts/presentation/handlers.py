from ninja import Path

from app.internal.bank_accounts.presentation.entities import BankAccountOut
from app.internal.exceptions.exceptions import ForbiddenException, NotFoundException


class BankAccountHandlers:
    def __init__(self, bank_account_repository):
        self._bank_account_repo = bank_account_repository

    def get_bank_account(self, request, account_number: str = Path(...)):
        bank_account = self._bank_account_repo.get_bank_account(account_number)
        if not bank_account:
            raise NotFoundException(account_number, "bank_account")

        if bank_account.owner.id != request.user_id:
            raise ForbiddenException()

        return BankAccountOut.from_orm(bank_account)
