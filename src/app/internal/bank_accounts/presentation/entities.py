from ninja.orm import create_schema

from app.internal.bank_accounts.db.models import BankAccount

BankAccountSchema = create_schema(BankAccount, exclude=["owner"])


class BankAccountOut(BankAccountSchema):
    pass
