class BadRequestException(Exception):
    def __init__(self, message):
        super().__init__()
        self.message = message


class UnauthorizedException(Exception):
    def __init__(self):
        super().__init__()
        self.message = "Unauthorized"


class ForbiddenException(Exception):
    def __init__(self):
        super().__init__()
        self.message = "Permission denied"


class NotFoundException(Exception):
    def __init__(self, id, name):
        super().__init__()
        self.id = id
        self.name = name
