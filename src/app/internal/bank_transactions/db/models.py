from datetime import datetime

from django.db import models
from django.utils import timezone

from app.internal.bank_accounts.db.models import BankAccount


class BankTransaction(models.Model):
    date = models.DateTimeField(default=timezone.now, blank=False, db_index=True)
    from_account = models.ForeignKey(
        BankAccount, blank=False, on_delete=models.CASCADE, related_name="transactions_from"
    )
    to_account = models.ForeignKey(BankAccount, blank=False, on_delete=models.CASCADE, related_name="transactions_to")
    amount = models.DecimalField(blank=False, max_digits=20, decimal_places=2)

    def __str__(self):
        return f"{self.date}: {self.amount} from {self.from_account} to {self.to_account}"

    class Meta:
        verbose_name = "Bank Transaction"
        verbose_name_plural = "Bank Transactions"
