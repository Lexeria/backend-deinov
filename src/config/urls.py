import environ
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path

from app.internal.app import get_api

env = environ.Env(
    SECRET_ADMIN_URL=(str, ""),  # TODO: импортировать из конфигурации
)

api = get_api()

urlpatterns = [
    path(env("SECRET_ADMIN_URL") + "/admin/", admin.site.urls),
    path("api/", api.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
